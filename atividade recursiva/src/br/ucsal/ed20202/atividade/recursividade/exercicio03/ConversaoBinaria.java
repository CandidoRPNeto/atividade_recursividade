package br.ucsal.ed20202.atividade.recursividade.exercicio03;

public class ConversaoBinaria {
	
	static String valor = "";
	
	public static int converterValor(int x) {
		Integer result = 0;

		valor += retornarZeroUm(x);
		
		if(x == 1) {
			result = Integer.parseInt(new StringBuilder(valor).reverse().toString());
			valor = "";
			return result;
		}
		
		return converterValor((int) (x / 2));
	}

	private static String retornarZeroUm(int x) {
		int result = 0;
		
		if(x % 2 == 0)
			result = 0;
		else if(x % 2 == 1)
			result = 1;
		
		return result + "";
	}
	
}
