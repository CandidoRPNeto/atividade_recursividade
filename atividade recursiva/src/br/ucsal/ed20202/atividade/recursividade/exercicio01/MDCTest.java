package br.ucsal.ed20202.atividade.recursividade.exercicio01;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

class MDCTest extends TestCase{

	@Test
	void testCalculaMDC() {
		assertEquals(1 , MDC.calculaMDC(854,673));
		assertEquals(3 , MDC.calculaMDC(12,9));
		assertEquals(5 , MDC.calculaMDC(10,5));
		assertEquals(30 , MDC.calculaMDC(30,30));
	}
}
