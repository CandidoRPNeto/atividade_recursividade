package br.ucsal.ed20202.atividade.recursividade.exercicio01;

public class MDC {

	static int r;
	
	public static int calculaMDC(int m, int n) {
		r = m % n;
		m = n;
		n = r;
			
		if (r == 0)
			return m;
		else
			return calculaMDC(m , n);
	}
	
}
