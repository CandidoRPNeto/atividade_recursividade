package br.ucsal.ed20202.atividade.recursividade.exercicio02;

public class Fatorial {

	static int r;
	
	public static int calculaFatorial(int x) {
		if(x == 1)
			return 1;
		
		return x *= calculaFatorial(x-1);
	}
	
}
