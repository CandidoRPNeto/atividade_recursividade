package br.ucsal.ed20202.atividade.recursividade.exercicio02;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;

class FatorialTest extends TestCase{

	@Test
	void testCalculaMDC() {
		assertEquals(2 , Fatorial.calculaFatorial(2));
		assertEquals(39916800 , Fatorial.calculaFatorial(11));		
		assertEquals(120 , Fatorial.calculaFatorial(5));
	}
}
